import { Button, FormControl, FormLabel, Input, InputGroup, InputRightElement, VStack } from '@chakra-ui/react';

import React, { useState } from 'react';
import { useToast } from '@chakra-ui/react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

const SignUp = () => {
  const navigate = useNavigate();

  const [showPass, setShowPass] = useState(false);
  const [showConfirmPass, setShowConfirmPass] = useState(false);

  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [pic, setPic] = useState();

  const [picLoading, setPicLoading] = useState(false);
  const toast = useToast();

  async function submidHanler() {
    setPicLoading(true);

    if (!name || !email || !password || !confirmPassword) {
      toast({
        title: 'Please fill all the fields!',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      setPicLoading(false);
      return;
    }

    if (password !== confirmPassword) {
      toast({
        title: 'Passwords Do Not Match',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      return setPicLoading(false);
    }

    try {
      const config = {
        headers: {
          'Content-type': 'application/json',
        },
      };

      const { data } = await axios.post(
        '/api/user/registration',

        {
          name,
          email,
          password,
          pic,
        },
        config
      );

      toast({
        title: 'Registration Successful',
        status: 'success',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });

      localStorage.setItem('userInfo', JSON.stringify(data));
      setPicLoading(false);

      navigate('/chats');
    } catch (error) {
      toast({
        title: 'Error Occured!',
        description: error.response.data.message,
        status: 'error',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      setPicLoading(false);
    }
  }

  // end of submit handler

  const postDetails = (pics) => {
    setPicLoading(true);
    if (pic === undefined) {
      toast({
        title: 'Select Image!.',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
    }

    if (pics.type === 'image/jpeg' || pics.type === 'image/png') {
      const data = new FormData();
      data.append('file', pics);
      data.append('upload_preset', 'chat-foneAPI');
      data.append('cloud_name', 'dbed2fwkj');
      fetch('https://api.cloudinary.com/v1_1/dbed2fwkj/image/upload', {
        method: 'post',
        body: data,
      })
        .then((res) => res.json())
        .then((data) => {
          setPic(data.url.toString());
          //   console.log(data);
          setPicLoading(false);
        })
        .catch((error) => {
          //   console.log(error);
          setPicLoading(false);
        });
    } else {
      toast({
        title: 'Please Select an Image!',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      setPicLoading(false);
      return;
    }
  };
  return (
    <VStack spacing={'5px'}>
      <FormControl id="firsttName">
        <Input placeholder="Enter Your Name" onChange={(e) => setName(e.target.value)} />
      </FormControl>

      <FormControl id="email">
        <Input placeholder="Enter Your Email" onChange={(e) => setEmail(e.target.value)} />
      </FormControl>

      <FormControl id="password">
        <InputGroup>
          <Input
            type={showPass ? 'text' : 'password'}
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <InputRightElement width={'4.5rem'}>
            <Button onClick={() => setShowPass((previous) => !previous)} h={'1.5rem'} size={'sm'}>
              {showPass ? 'Hide' : 'Show'}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <FormControl id="confirmPassword">
        <InputGroup>
          <Input
            type={showConfirmPass ? 'text' : 'password'}
            placeholder="Confirm Password"
            onChange={(e) => setConfirmPassword(e.target.value)}
          />
          <InputRightElement width={'4.5rem'}>
            <Button onClick={() => setShowConfirmPass((prev) => !prev)} h={'1.5rem'} size={'sm'}>
              {showConfirmPass ? 'Hide' : 'Show'}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <FormControl id="pic">
        <FormLabel>Upload your picture</FormLabel>
        <Input type="file" p={1.5} accept="image/*" onChange={(e) => postDetails(e.target.files[0])} />
      </FormControl>

      <Button colorScheme="teal" width={'100%'} style={{ marginTop: 15 }} onClick={submidHanler} isLoading={picLoading}>
        Sign Up
      </Button>
    </VStack>
  );
};

export default SignUp;
