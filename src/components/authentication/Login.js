import { Button, FormControl, Input, InputGroup, InputRightElement, VStack } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useToast } from '@chakra-ui/react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
const Login = () => {
  const toast = useToast();
  const navigate = useNavigate();

  const [showPass, setShowPass] = useState(false);

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);

  async function submidHanler() {
    // eslint-disable-next-line react-hooks/rules-of-hooks

    setLoading(true);

    if (!email || !password) {
      toast({
        title: 'Please fill all the fields!',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      setLoading(false);
      return;
    }

    try {
      const config = {
        headers: {
          'Content-type': 'application/json',
        },
      };

      const { data } = await axios.post(
        '/api/user/login',
        {
          email,
          password,
        },
        config
      );

      toast({
        title: 'Login Successful',
        status: 'success',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });

      localStorage.setItem('userInfo', JSON.stringify(data));

      setLoading(false);
      navigate('/chats');
    } catch (error) {
      toast({
        title: 'Error Occured!',
        description: error.response.data.message,
        status: 'error',
        duration: 1500,
        isClosable: true,
        position: 'bottom',
      });
      setLoading(false);
    }
  }
  return (
    <VStack spacing={'5px'}>
      <FormControl id="emailLogin">
        <Input placeholder="Enter Your Email" onChange={(e) => setEmail(e.target.value)} />
      </FormControl>

      <FormControl id="passwordLogin">
        <InputGroup>
          <Input
            type={showPass ? 'text' : 'password'}
            placeholder="Password"
            onChange={(e) => setPassword(e.target.value)}
          />
          <InputRightElement width={'4.5rem'}>
            <Button onClick={() => setShowPass((previous) => !previous)} h={'1.5rem'} size={'sm'}>
              {showPass ? 'Hide' : 'Show'}
            </Button>
          </InputRightElement>
        </InputGroup>
      </FormControl>

      <Button colorScheme="teal" width={'100%'} style={{ marginTop: 15 }} onClick={submidHanler} isLoading={loading}>
        Login
      </Button>
    </VStack>
  );
};

export default Login;
