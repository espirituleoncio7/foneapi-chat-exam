import React from 'react';
import { useToast } from '@chakra-ui/toast';
import axios from 'axios';
import { useChatContext } from '../contextAPI/chatProvider';
import { useEffect, useState } from 'react';
import { Avatar, Box, Stack, Text } from '@chakra-ui/react';
import ChatLoading from './miscellaneous/ChatLoading';
import { getSender, getSenderPic } from './config/ChatLogics';

const MyChats = ({ fetchAgain }) => {
  const [loggedUser, setLoggedUser] = useState();
  const { selectedChat, setSelectedChat, user, chats, setChats } = useChatContext();
  const toast = useToast();

  const fetchChats = async () => {
    // console.log(user._id);
    try {
      const config = {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      };

      const { data } = await axios.get('/api/chat', config);

      setChats(data);
    } catch (error) {
      toast({
        title: 'Error Occured!',
        description: 'Failed to Load the chats',
        status: 'error',
        duration: 1500,
        isClosable: true,
        position: 'bottom-left',
      });
    }
  };

  useEffect(() => {
    setLoggedUser(JSON.parse(localStorage.getItem('userInfo')));
    fetchChats();
    // eslint-disable-next-line
  }, [fetchAgain]);

  return (
    <Box
      display={{ base: selectedChat ? 'none' : 'flex', md: 'flex' }}
      flexDir={'column'}
      alignItems={'center'}
      p={3}
      w={{ base: '100%', md: '31%' }}
      bg="#F8F8F8"
      borderRadius="lg"
      borderWidth="1px"
      m={'10px 5px 10px 10px'}
    >
      <Box
        p={3}
        fontSize={'24px'}
        display={'flex'}
        w={'100%'}
        justifyContent={'center'}
        alignItems={'center'}
        bg={'#1c1e27'}
        color="silver"
        borderRadius="lg"
        borderWidth="1px"
      >
        <Text fontSize={'25px'} fontWeight={'bold'}>
          Contacts
        </Text>
      </Box>

      {/* new code */}
      <Box display="flex" flexDir="column" mt={3} w="100%" h="100%" borderRadius="lg" overflowY="hidden">
        {chats ? (
          <Stack overflowY="scroll">
            {loggedUser &&
              chats.map((chat) => (
                <Box
                  onClick={() => setSelectedChat(chat)}
                  cursor="pointer"
                  bg={selectedChat === chat ? '#b8e2df' : '#d4efed'}
                  color={selectedChat === chat ? 'teal' : 'teal'}
                  px={3}
                  py={2}
                  borderRadius="lg"
                  key={chat._id}
                  display={'flex'}
                  alignItems={'center'}
                  w={'100%'}
                >
                  <Avatar mr={2} size="sm" cursor="pointer" src={getSenderPic(loggedUser, chat.users)} />

                  <Box overflow={'hidden'}>
                    <Text fontWeight={'semibold'}>{getSender(loggedUser, chat.users)}</Text>
                    {chat.latestMessage && (
                      <Text fontSize="xs" fontWeight={'light'} fontStyle={'italic'}>
                        <b>{chat.latestMessage.sender.name} : </b>
                        {chat.latestMessage.content.length > 20
                          ? chat.latestMessage.content.substring(0, 20) + '  ...'
                          : chat.latestMessage.content}
                      </Text>
                    )}
                  </Box>
                </Box>
              ))}
          </Stack>
        ) : (
          <ChatLoading />
        )}
      </Box>
    </Box>
  );
};

export default MyChats;
