import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { Box, Button, Tooltip, Text, Menu, MenuButton, Avatar, MenuList } from '@chakra-ui/react';
import { MenuItem, Drawer, DrawerOverlay, DrawerContent, DrawerHeader, DrawerBody, Input } from '@chakra-ui/react';
import { useDisclosure } from '@chakra-ui/hooks';
import { Spinner } from '@chakra-ui/react';
import axios from 'axios';

import { BiSearchAlt2 as IconSearch } from 'react-icons/bi';
import { BsFillBellFill as IconBell, BsCaretDownFill as IconDown } from 'react-icons/bs';
import { useToast } from '@chakra-ui/toast';
// import NotificationBadge from 'react-notification-badge';
// import { Effect } from 'react-notification-badge';

import { useChatContext } from '../../contextAPI/chatProvider';
import ProfileModal from './Profile';
import ChatLoading from './ChatLoading';
import UserListItem from '../userProfile/UserListItem';
import { getSender } from '../config/ChatLogics';

const SideDrawer = () => {
  const toast = useToast();
  const { isOpen, onOpen, onClose } = useDisclosure();

  const navigate = useNavigate();
  const { setSelectedChat, user, chats, setChats, notification, setNotification } = useChatContext();

  const [search, setSearch] = useState('');
  const [searchResult, setSearchResult] = useState([]);
  const [loading, setLoading] = useState(false);
  const [loadingChat, setLoadingChat] = useState(false);

  function logOutHandler() {
    localStorage.removeItem('userInfo');
    navigate('/');
  }

  const handleSearch = async () => {
    if (!search) {
      toast({
        title: 'Please Enter something in search',
        status: 'warning',
        duration: 1500,
        isClosable: true,
        position: 'top-left',
      });
      return;
    }

    try {
      setLoading(true);

      const config = {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      };

      const { data } = await axios.get(`/api/user/searchUser?search=${search}`, config);

      setLoading(false);
      setSearchResult(data);
    } catch (error) {
      toast({
        title: 'Error Occured!',
        description: 'Failed to Load the Search Results',
        status: 'error',
        duration: 1500,
        isClosable: true,
        position: 'bottom-left',
      });
    }
  };

  const accessChat = async (userId) => {
    // console.log(userId);

    try {
      setLoadingChat(true);
      const config = {
        headers: {
          'Content-type': 'application/json',
          Authorization: `Bearer ${user.token}`,
        },
      };
      const { data } = await axios.post(`/api/chat`, { userId }, config);

      if (!chats.find((c) => c._id === data._id)) {
        setChats([data, ...chats]);
      }
      setSelectedChat(data);
      setLoadingChat(false);
      onClose();
    } catch (error) {
      toast({
        title: 'Error fetching chat',
        description: error.message,
        status: 'error',
        duration: 1500,
        isClosable: true,
        position: 'bottom-left',
      });
    }
  };

  return (
    <>
      <Box display="flex" justifyContent="space-between" alignItems="center" bg={'teal'} w="100%" p="10px">
        <Tooltip label="Search User to chat" hasArrow placement="bottom-end">
          <Button variant="solid" colorScheme="gray" onClick={onOpen}>
            <IconSearch fontSize={'20px'} color="gray" />
            <Text display={{ base: 'none', md: 'flex' }} px="4" color="gray">
              Search Contact
            </Text>
          </Button>
        </Tooltip>

        <div style={{ display: 'flex', gap: '10px' }}>
          <Menu>
            <MenuButton p={1} style={{ position: 'relative' }}>
              {/* <NotificationBadge
                style={{ backgroundColor: 'orange' }}
                count={notification.length}
                effect={Effect.SCALE}
              /> */}
              {notification.length > 0 && (
                <div
                  style={{
                    backgroundColor: 'red',
                    width: '12px',
                    height: '12px',
                    borderRadius: '10px',
                    position: 'absolute',
                    top: '5px',
                    right: '5px',
                  }}
                ></div>
              )}
              <IconBell color="white" fontSize={'25px'} />
            </MenuButton>

            <MenuList p={2} className="notification-code">
              {!notification.length && <Text>No new message</Text>}
              {notification.map((notif, index) => (
                <MenuItem
                  key={index}
                  onClick={() => {
                    setSelectedChat(notif.chat);
                    setNotification(notification.filter((n) => n !== notif));
                  }}
                >
                  {`New Message from ${getSender(user, notif.chat.users)}`}
                </MenuItem>
              ))}
            </MenuList>
          </Menu>

          <Menu>
            <MenuButton as={Button} rightIcon={<IconDown color="teal" />}>
              <Avatar size={'sm'} cursor="pointer" name={user.name} src={user.pic} />
            </MenuButton>

            <MenuList>
              <ProfileModal user={user}>
                <MenuItem>View Profile</MenuItem>
              </ProfileModal>
              <MenuItem onClick={logOutHandler}>Logout</MenuItem>
            </MenuList>
          </Menu>
        </div>
      </Box>

      <Drawer placement="left" onClose={onClose} isOpen={isOpen}>
        <DrawerOverlay />
        <DrawerContent>
          <DrawerHeader borderBottomWidth="1px">Search Contact</DrawerHeader>
          <DrawerBody>
            <Box style={{ display: 'flex', paddingBottom: '15px' }}>
              <Input placeholder="Search name or email" value={search} onChange={(e) => setSearch(e.target.value)} />
              <Button onClick={handleSearch}>
                <IconSearch fontSize={'20px'} color="gray" />
              </Button>
            </Box>
            {loading ? (
              <ChatLoading />
            ) : (
              searchResult?.map((user) => (
                <UserListItem key={user._id} user={user} handleFunction={() => accessChat(user._id)} />
              ))
            )}
            {loadingChat && <Spinner ml="auto" d="flex" />}
          </DrawerBody>
        </DrawerContent>
      </Drawer>
    </>
  );
};

export default SideDrawer;
