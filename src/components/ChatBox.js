import { Box } from '@chakra-ui/layout';
import SingleChat from './SingleChat';
import { useChatContext } from '../contextAPI/chatProvider';
// import './styles.css';

const Chatbox = ({ fetchAgain, setFetchAgain }) => {
  const { selectedChat } = useChatContext();

  return (
    <Box
      display={{ base: selectedChat ? 'flex' : 'none', md: 'flex' }}
      alignItems="center"
      flexDir="column"
      p={3}
      bg="#F8F8F8"
      w={{ base: '100%', md: '68%' }}
      borderRadius="lg"
      borderWidth="1px"
      m={'10px 10px 10px 5px'}
    >
      <SingleChat fetchAgain={fetchAgain} setFetchAgain={setFetchAgain} />
    </Box>
  );
};

export default Chatbox;
